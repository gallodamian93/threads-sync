
public class main {

	public static void main(String[] args) {
		Cuenta c = new Cuenta();
		
		Extractor e = new Extractor(c, "cliente 1");
		Extractor e2 = new Extractor(c, "cliente 2");
		Extractor e3 = new Extractor(c, "cliente 3");
		
		Depositador d = new Depositador(c, "cliente 4");
		Depositador d2 = new Depositador(c, "cliente 5");
		Depositador d3 = new Depositador(c, "cliente 6");

		
		Thread exThread = new Thread(e);
		Thread exThread2 = new Thread(e2);
		Thread exThread3 = new Thread(e3);
		
		Thread deThread = new Thread(d);
		Thread deThread2 = new Thread(d2);
		Thread deThread3 = new Thread(d3);

		
		exThread.start();
		exThread2.start();
		exThread3.start();
		deThread.start();
		deThread2.start();
		deThread3.start();

	}

}
