
public class Cuenta {
	int saldo = 30;
	
	public void depositar(int cantidad) throws InterruptedException{
		synchronized(this){
			Thread.sleep(40);
			int mySaldo = this.saldo;
			/*dormir thread aca para generar inconsistencias ya que el valor del saldo
			*lo puede estar modificando otro
			*/
			//Thread.sleep(2000);
			saldo = mySaldo + cantidad;
			System.out.println("	Se depositaron: " + cantidad);
			System.out.println("Saldo actual: " + saldo);
		}
	}
	
	public void extraer(int cantidad) throws InterruptedException{
		synchronized(this) {
			Thread.sleep(80);
			if ((this.saldo - cantidad) < 0) {
				System.out.println("Error: fondos insuficientes");
			} else {
				int mySaldo = this.saldo;
				/*
				 * para forzar el error hay que poner un sleep en esta linea (tambien en 
				 * la de depositar), ya que en este punto se verifico que la extraccion se podra hacer
				 * y se va a hacer a continuacion. De esta manera, teniendo al thread
				 * dormido otro thread puede modificar el saldo y cuando este se despierte
				 * tambien lo hara generando inconsistencias
				 */
				//Thread.sleep(2000);
				this.saldo = mySaldo - cantidad;
				System.out.println("	Se extrayeron: " + cantidad);
				System.out.println("Saldo actual: " + saldo);
			}
		}
	}
}
