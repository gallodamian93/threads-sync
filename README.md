## Sobre el proyecto

Un banco tiene un proceso para realizar depósitos en cuentas, y otro para
extracciones. Ambos procesos corren en simultáneo y aceptan varios clientes a la vez.
El proceso que realiza un depósito tarda 40 mseg entre que consulta el saldo actual, y
lo actualiza con el nuevo valor. El proceso que realiza una extracción tarda 80 mseg
entre que consulta el saldo (y verifica que haya disponible) y lo actualiza con el nuevo
valor.

## Instrucciones

1. Correr main.java: en la clase Cuenta está comentado cómo haría para forzar un error